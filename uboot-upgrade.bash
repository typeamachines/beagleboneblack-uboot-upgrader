#!/bin/bash

set -e

# This script runs on the beaglebone. It downloads the uBoot files,
# mounts the onboard flash, and copies the files onto it. Reboots upon
# success.

die() {	echo "$@" 1>&2;	exit 1; }

prefix=$(mktemp -d uboot-upgrade-XXXXXXXX) ||
die "couldn't create temp dir"

cd "$prefix"
mountpoint="bootdisk"

wget "https://bitbucket.org/typeamachines/u-boot/downloads/u-boot.img" ||
die "Couldn't download uBoot image"

wget "https://bitbucket.org/typeamachines/u-boot/downloads/MLO" ||
die "Couldn't download MLO"

echo "creating mount point";
mkdir -p "${mountpoint}"
mount -o rw /dev/mmcblk1p1 "${mountpoint}"
md5sum "${mountpoint}"/{MLO,u-boot.img}

mv "${mountpoint}/MLO" "${mountpoint}/MLO.original"
mv "${mountpoint}/u-boot.img" "${mountpoint}/u-boot.img.original"
mv MLO "${mountpoint}/"
mv u-boot.img "${mountpoint}/"

umount "${mountpoint}"

echo "rebooting your Series 1"
/sbin/reboot
