#!/bin/bash

# This script copies a bootloader update script to a beaglebone black
# running the 2014 Series 1 3D printer software in order to fix the
# u-boot hang bug. See
# https://groups.google.com/forum/#!topic/beagleboard/mlxRz0bSHkI

# it's assumed the default user 'ubuntu' is used on the 2014 Series 1
# user will have to supply the password

if [[ "$#" -ne 1 ]] || [[ $1 == -* ]]; then
	echo "usage: $0 <hostname>"
	exit 0
fi

host="$1"
scp uboot-upgrade.bash "ubuntu@${host}:/tmp/"
ssh -t "ubuntu@${host}" "sudo bash /tmp/uboot-upgrade.bash"
