files = README.md README.html deploy-uboot-upgradescript.bash \
uboot-upgrade.bash copypaste.txt


VERSION = $(shell git describe)
DIST = uboot-upgrade

all: zip

clean:
	-rm $(DIST)_$(VERSION).zip copypaste.txt README.html

%.txt: %.txt.in
	m4 -DVERSION=${VERSION} -P $< > $@

%.html : %.md
	markdown $< > $@

zip: $(DIST)_$(VERSION).zip

$(DIST)_$(VERSION).zip: $(DIST)_$(VERSION)

$(DIST)_$(VERSION): $(files)
	[ -d $(DIST)_$(VERSION) ] || \
mkdir $(DIST)_$(VERSION)
	cp $(files) $(DIST)_$(VERSION)


%.zip: %
	zip -r  $@ $<
